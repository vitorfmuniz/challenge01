//
//  ExercicioCifraViewController.h
//  Challenge01
//
//  Created by Vitor Muniz on 10/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModalResultadoViewController.h"

@interface ExercicioCifraViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *doNoteName;
@property (weak, nonatomic) IBOutlet UIButton *reNoteName;
@property (weak, nonatomic) IBOutlet UIButton *miNoteName;
@property (weak, nonatomic) IBOutlet UIButton *faNoteName;
@property (weak, nonatomic) IBOutlet UIButton *solNoteName;
@property (weak, nonatomic) IBOutlet UIButton *laNoteName;
@property (weak, nonatomic) IBOutlet UIButton *siNoteName;

@property (weak, nonatomic) IBOutlet UIButton *doSymbolNote;
@property (weak, nonatomic) IBOutlet UIButton *reSymbolNote;
@property (weak, nonatomic) IBOutlet UIButton *miSymbolNote;
@property (weak, nonatomic) IBOutlet UIButton *faSymbolNote;
@property (weak, nonatomic) IBOutlet UIButton *solSymbolNote;
@property (weak, nonatomic) IBOutlet UIButton *laSymbolNote;
@property (weak, nonatomic) IBOutlet UIButton *siSymbolNote;

@property (weak, nonatomic) IBOutlet UIButton *botaoContinuar;

@property UIButton *botaoSelecionado1;
@property int numAcertos;
@property int numErros;
@property int cont;


-(void)configurarBotoes: (UIButton*)botao;

-(void)configurarBotoesAoAcertar: (UIButton*)botao1 andBotao: (UIButton*)botao2;

- (IBAction)selecionarBotao:(UIButton *)sender;

- (IBAction)continuar:(UIButton *)sender;

-(void) erroJogoDaMemoria: (UIButton *)sender andBotaoSelecionado: (UIButton *) botaoSelecionado;


@end
