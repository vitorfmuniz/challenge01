//
//  ExercicioCapturaDeSom1ViewController.h
//  Challenge01
//
//  Created by Vitor Muniz on 09/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <EZAudio/EZAudio.h>
#import <MediaPlayer/MediaPlayer.h>
#import "ReproducaoAudio.h"
#import "Fase1.h"
#import "ModalResultadoViewController.h"

@import AudioKit;
@import AudioKitUI;

@interface ExercicioCapturaDeSom1ViewController : UIViewController <EZMicrophoneDelegate,EZAudioFFTDelegate,EZOutputDataSource,EZOutputDelegate>

@property (strong, nonatomic) EZMicrophone * microphone;
@property (strong, nonatomic) EZAudioFFTRolling *fft;

@property (weak, nonatomic) IBOutlet UILabel *noteLabel;
@property (weak, nonatomic) IBOutlet UIButton *buttonNoteReferenceOutlet;
@property (weak, nonatomic) IBOutlet UILabel *labelNotaCerta;
@property (weak, nonatomic) IBOutlet UIButton *buttonContinuar;
@property (nonatomic,weak) IBOutlet EZAudioPlot *audioPlotTime;
@property (weak, nonatomic) IBOutlet UIButton *botaoTenteNovamente;

@property (strong,nonatomic) NSString *noteName;
@property ReproducaoAudio *reproducaoAudio;
@property int cont;
@property NSString *respostaCerta;
@property NSArray *nomeNotas; // self.nomeNotasCifras
@property Fase1 *fase1;
@property NSString *notaAtual;
@property int indice;
@property NSArray * nomeNotasEscritas ;//self.nomeNotas
@property NSUInteger indexRespostaCerta;
@property NSArray* nomeNotasSuten;
@property NSArray* nomeNotasEscritasSuten;
@property int numAcertos;
@property int numErros;
@property int numeroDaFase;
@property BOOL isFetchingAudio;
@property MPVolumeView *mpVolumeView;
@property (nonatomic, strong) EZAudioDevice *output;



-(void)compararNotas;

- (IBAction)buttonNoteReference:(UIButton *)sender;

- (IBAction)continuarAction:(UIButton *)sender;

- (IBAction)tentarNovamente:(UIButton *)sender;


@end
