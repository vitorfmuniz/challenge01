//
//  TelaDeSonsFase1ViewController.m
//  Challenge01
//
//  Created by Vitor Muniz on 05/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import "TelaDeSonsFase1ViewController.h"

@interface TelaDeSonsFase1ViewController () 




@end

@implementation TelaDeSonsFase1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [self carregarSons];
    
    self.reproducaoAudio = [[ReproducaoAudio alloc] initWithArray:@[@"C",@"D",@"E",@"F",@"G",@"A",@"B"] andNumeroDaOitava:@"2"];

}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)reproduzirSom:(UIButton *)sender {

    [self.reproducaoAudio playAudio:sender.tag];
}

//-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    
//    Exercicio1SonsViewController *exercicioViewController = [segue destinationViewController];
//    exercicioViewController.reproducaoAudio = self.reproducaoAudio;
//}

//-(void) carregarSons{
//    NSURL *doURL = [[NSBundle mainBundle] URLForResource:@"3C" withExtension:@"wav" subdirectory:@"PIANO KEYS"];
//    AVAudioPlayer *doMusicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:doURL error:nil];
//    //doMusicPlayer.numberOfLoops = 1;
//    //[doMusicPlayer playAtTime:1.0];
//    
//    NSURL *reURL = [[NSBundle mainBundle] URLForResource:@"3D" withExtension:@"wav" subdirectory:@"PIANO KEYS"];
//    AVAudioPlayer *reMusicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:reURL error:nil];
//    //reMusicPlayer.numberOfLoops = 1;
//    
//    NSURL *miURL = [[NSBundle mainBundle] URLForResource:@"3E" withExtension:@"wav" subdirectory:@"PIANO KEYS"];
//    AVAudioPlayer *miMusicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:miURL error:nil];
//    //miMusicPlayer.numberOfLoops = 1;
//    
//    NSURL *faURL = [[NSBundle mainBundle] URLForResource:@"3F" withExtension:@"wav" subdirectory:@"PIANO KEYS"];
//    AVAudioPlayer *faMusicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:faURL error:nil];
//    //faMusicPlayer.numberOfLoops = 1;
//    
//    NSURL *solURL = [[NSBundle mainBundle] URLForResource:@"3G" withExtension:@"wav" subdirectory:@"PIANO KEYS"];
//    AVAudioPlayer *solMusicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:solURL error:nil];
//    //solMusicPlayer.numberOfLoops = 1;
//    
//    NSURL *laURL = [[NSBundle mainBundle] URLForResource:@"3A" withExtension:@"wav" subdirectory:@"PIANO KEYS"];
//    AVAudioPlayer *laMusicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:laURL error:nil];
//    //laMusicPlayer.numberOfLoops = 1;
//    
//    NSURL *siURL = [[NSBundle mainBundle] URLForResource:@"3B" withExtension:@"wav" subdirectory:@"PIANO KEYS"];
//    AVAudioPlayer *siMusicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:siURL error:nil];
//    //siMusicPlayer.numberOfLoops = 1;
//    
//    self.somDasNotas  = [[NSArray alloc ]initWithObjects:doMusicPlayer, reMusicPlayer, miMusicPlayer, faMusicPlayer, solMusicPlayer,laMusicPlayer,siMusicPlayer,nil];
//}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/





@end
