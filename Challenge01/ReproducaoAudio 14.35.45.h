//
//  ReproducaoAudio.h
//  Challenge01
//
//  Created by Vitor Muniz on 11/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
@interface ReproducaoAudio : NSObject <AVAudioPlayerDelegate>
@property NSMutableArray *sons;
@property NSInteger index;


-(void)playAudio:(NSInteger) index;
-(void)resetAudio:(NSInteger) index;
- (instancetype)initWithArray:(NSArray <NSString *> *) array andNumeroDaOitava: (NSString *)numeroDaOitava;

@end
