//
//  ExercicioCifraViewController.m
//  Challenge01
//
//  Created by Vitor Muniz on 10/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import "ExercicioCifraViewController.h"

@interface ExercicioCifraViewController ()

@end

@implementation ExercicioCifraViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //configurando botoes
    
    self.cont = 0;
    
    [self.botaoContinuar setHidden:YES];
    
    [self configurarBotoes:self.doNoteName];
    [self configurarBotoes:self.reNoteName];
    [self configurarBotoes:self.miNoteName];
    [self configurarBotoes:self.faNoteName];
    [self configurarBotoes:self.solNoteName];
    [self configurarBotoes:self.laNoteName];
    [self configurarBotoes:self.siNoteName];
    
    [self configurarBotoes:self.doSymbolNote];
    [self configurarBotoes:self.reSymbolNote];
    [self configurarBotoes:self.miSymbolNote];
    [self configurarBotoes:self.faSymbolNote];
    [self configurarBotoes:self.solSymbolNote];
    [self configurarBotoes:self.laSymbolNote];
    [self configurarBotoes:self.siSymbolNote];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)configurarBotoes: (UIButton*)botao{
    
    botao.layer.borderWidth = 2;
    botao.layer.cornerRadius = 8;
    botao.layer.borderColor = [[UIColor grayColor] CGColor];
    [botao setTitleColor:[UIColor colorWithRed:255.0/255.0 green:140.0/255.0 blue:0.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    botao.backgroundColor = [UIColor whiteColor];
    
}

-(void)configurarBotoesAoAcertar: (UIButton*)botao1 andBotao: (UIButton*)botao2{
    [botao1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    botao1.backgroundColor = [UIColor colorWithRed:32.0/255.0 green:176.0/255.0 blue:87.0/255.0 alpha:1.0];
    
    [botao2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    botao2.backgroundColor = [UIColor colorWithRed:32.0/255.0 green:176.0/255.0 blue:87.0/255.0 alpha:1.0];
    
    //UIColor *vermelho = [UIColor colorWithRed: 227.0/255.0 green:69.0/255.0 blue:34.0/255.0 alpha:1.0];
   // UIColor *verde = [UIColor colorWithRed:32.0/255.0 green:176.0/255.0 blue:87.0/255.0 alpha:1.0];
    
}

-(void)configurarBotoesSelecionados: (UIButton*) botao{
    botao.backgroundColor = [UIColor grayColor];
    [botao setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (IBAction)continuar:(UIButton *)sender {

        [self performSegueWithIdentifier:@"segueResultadoJogoDaMem" sender:self];

}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString: @"segueResultadoJogoDaMem"]) {
        ModalResultadoViewController *modalResultado = [segue destinationViewController];
        NSLog(@"%d, %d", self.numAcertos, self.numErros);
        modalResultado.numAcertos = self.numAcertos;
        modalResultado.numErros = self.numErros;
    }
    
}

-(void) erroJogoDaMemoria: (UIButton *)sender andBotaoSelecionado: (UIButton *) botaoSelecionado {
    self.numErros ++;
    [sender setEnabled:NO];
    
    [UIView animateWithDuration:0.4 animations:^{
        [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        botaoSelecionado.backgroundColor = [UIColor redColor];
        sender.backgroundColor = [UIColor redColor];
        
    } completion:^(BOOL finished) {
        [self configurarBotoes: botaoSelecionado];
        [self configurarBotoes: sender];
    }];
    
    [botaoSelecionado setEnabled:YES];
    [sender setEnabled:YES];
}


- (IBAction)selecionarBotao:(UIButton *)sender {

    NSLog(@"ace:%d er:%d", self.numAcertos,self.numErros);
    
    if(self.cont == 0){
        [sender setEnabled:NO];
        self.botaoSelecionado1 = sender;
        [self configurarBotoesSelecionados:sender];
        self.cont ++;
        
    }else if (self.cont == 1){
    
        self.cont ++;
        
        if ([sender isEqual: self.doNoteName]){ // NOTA DO ESCRITA
            
            if([self.botaoSelecionado1 isEqual:self.doSymbolNote]){
                self.numAcertos ++;
                [self configurarBotoesAoAcertar:sender andBotao:self.doSymbolNote];
                [sender setEnabled:NO];
                [self.doSymbolNote setEnabled:NO];
                
            }else{
                [self erroJogoDaMemoria:sender andBotaoSelecionado:self.botaoSelecionado1];
            }
            
        }else  if ([sender isEqual: self.doSymbolNote]){ // NOTA DO SIMBOLO
            
            if([self.botaoSelecionado1 isEqual:self.doNoteName]){
                self.numAcertos ++;
                [self configurarBotoesAoAcertar:sender andBotao:self.doNoteName];
                [sender setEnabled:NO];
                [self.doNoteName setEnabled:NO];
                
            }else{
                [self erroJogoDaMemoria:sender andBotaoSelecionado:self.botaoSelecionado1];
            }
            
        }else  if ([sender isEqual: self.reNoteName]){ // NOTA RE ESCRITA
            
            if([self.botaoSelecionado1 isEqual:self.reSymbolNote]){
                self.numAcertos ++;
                [self configurarBotoesAoAcertar:sender andBotao:self.reSymbolNote];
                [sender setEnabled:NO];
                [self.reSymbolNote setEnabled:NO];
                
            }else{
                [self erroJogoDaMemoria:sender andBotaoSelecionado:self.botaoSelecionado1];
            }
            
        }else  if ([sender isEqual: self.reSymbolNote]){ // NOTA RE SIMBOLO
            
            if([self.botaoSelecionado1 isEqual:self.reNoteName]){
                self.numAcertos ++;
                [self configurarBotoesAoAcertar:sender andBotao:self.reNoteName];
                [sender setEnabled:NO];
                [self.reNoteName setEnabled:NO];
                
            }else{
                [self erroJogoDaMemoria:sender andBotaoSelecionado:self.botaoSelecionado1];
            }
            
        }else  if ([sender isEqual: self.miNoteName]){ // NOTA MI ESCRITA
            
            if([self.botaoSelecionado1 isEqual:self.miSymbolNote]){
                self.numAcertos ++;
                [self configurarBotoesAoAcertar:sender andBotao:self.miSymbolNote];
                [sender setEnabled:NO];
                [self.miSymbolNote setEnabled:NO];
                
            }else{
                [self erroJogoDaMemoria:sender andBotaoSelecionado:self.botaoSelecionado1];
            }
            
        }else  if ([sender isEqual: self.miSymbolNote]){ // NOTA MI SIMBOLO
            
            if([self.botaoSelecionado1 isEqual:self.miNoteName]){
                self.numAcertos ++;
                [self configurarBotoesAoAcertar:sender andBotao:self.miNoteName];
                [sender setEnabled:NO];
                [self.miNoteName setEnabled:NO];
                
            }else{
                [self erroJogoDaMemoria:sender andBotaoSelecionado:self.botaoSelecionado1];
            }
            
        }else  if ([sender isEqual: self.faNoteName]){ // NOTA FA ESCRITA
            
            if([self.botaoSelecionado1 isEqual:self.faSymbolNote]){
                self.numAcertos ++;
                [self configurarBotoesAoAcertar:sender andBotao:self.faSymbolNote];
                [sender setEnabled:NO];
                [self.faSymbolNote setEnabled:NO];
                
            }else{
                [self erroJogoDaMemoria:sender andBotaoSelecionado:self.botaoSelecionado1];
            }
            
        }else  if ([sender isEqual: self.faSymbolNote]){ // NOTA FA SIMBOLO
            if([self.botaoSelecionado1 isEqual:self.faNoteName]){
                self.numAcertos ++;
                [self configurarBotoesAoAcertar:sender andBotao:self.faNoteName];
                [sender setEnabled:NO];
                [self.faNoteName setEnabled:NO];
                
            }else{
                [self erroJogoDaMemoria:sender andBotaoSelecionado:self.botaoSelecionado1];
            }
            
        }else  if ([sender isEqual: self.solNoteName]){ // NOTA SOL ESCRITA
            
            if([self.botaoSelecionado1 isEqual:self.solSymbolNote]){
                self.numAcertos ++;
                [self configurarBotoesAoAcertar:sender andBotao:self.solSymbolNote];
                [sender setEnabled:NO];
                [self.solSymbolNote setEnabled:NO];
                
            }else{
                [self erroJogoDaMemoria:sender andBotaoSelecionado:self.botaoSelecionado1];
            }
            
        }else  if ([sender isEqual: self.solSymbolNote]){ // NOTA SOL SIMBOLO
            
            if([self.botaoSelecionado1 isEqual:self.solNoteName]){
                self.numAcertos ++;
                [self configurarBotoesAoAcertar:sender andBotao:self.solNoteName];
                [sender setEnabled:NO];
                [self.solNoteName setEnabled:NO];
                
            }else{
                [self erroJogoDaMemoria:sender andBotaoSelecionado:self.botaoSelecionado1];
            }
            
        }else  if ([sender isEqual: self.laNoteName]){ // NOTA LA ESCRITA
            
            if([self.botaoSelecionado1 isEqual:self.laSymbolNote]){
                self.numAcertos ++;
                [self configurarBotoesAoAcertar:sender andBotao:self.laSymbolNote];
                [sender setEnabled:NO];
                [self.laSymbolNote setEnabled:NO];
                
            }else{
                [self erroJogoDaMemoria:sender andBotaoSelecionado:self.botaoSelecionado1];
            }
            
        }else  if ([sender isEqual: self.laNoteName]){ // NOTA LA ESCRITA
            
            if([self.botaoSelecionado1 isEqual:self.laSymbolNote]){
                self.numAcertos ++;
                [self configurarBotoesAoAcertar:sender andBotao:self.laSymbolNote];
                [sender setEnabled:NO];
                [self.laSymbolNote setEnabled:NO];
                
            }else{
                [self erroJogoDaMemoria:sender andBotaoSelecionado:self.botaoSelecionado1];
            }
            
        }else  if ([sender isEqual: self.laSymbolNote]){ // NOTA LA SIMBOLO
            
            if([self.botaoSelecionado1 isEqual:self.laNoteName]){
                self.numAcertos ++;
                [self configurarBotoesAoAcertar:sender andBotao:self.laNoteName];
                [sender setEnabled:NO];
                [self.laNoteName setEnabled:NO];
                
            }else{
                [self erroJogoDaMemoria:sender andBotaoSelecionado:self.botaoSelecionado1];
            }
            
        }else  if ([sender isEqual: self.siNoteName]){ // NOTA SI ESCRITA
            
            if([self.botaoSelecionado1 isEqual:self.siSymbolNote]){
                self.numAcertos ++;
                [self configurarBotoesAoAcertar:sender andBotao:self.siSymbolNote];
                [sender setEnabled:NO];
                [self.siSymbolNote setEnabled:NO];
                
            }else{
                [self erroJogoDaMemoria:sender andBotaoSelecionado:self.botaoSelecionado1];
            }
            
        }else  if ([sender isEqual: self.siSymbolNote]){ // NOTA SI SIMBOLO
            
            if([self.botaoSelecionado1 isEqual:self.siNoteName]){
                self.numAcertos ++;
                [self configurarBotoesAoAcertar:sender andBotao:self.siNoteName];
                [sender setEnabled:NO];
                [self.siNoteName setEnabled:NO];
                
            }else{
                [self erroJogoDaMemoria:sender andBotaoSelecionado:self.botaoSelecionado1];
            }
            
        }

        
        self.cont = 0;
            
        }

    if (self.numAcertos == 7) {
        [self.botaoContinuar setHidden:NO];
    }

}




-(BOOL)prefersStatusBarHidden{
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
