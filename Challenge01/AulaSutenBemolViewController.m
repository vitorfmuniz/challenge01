//
//  AulaSutenBemolViewController.m
//  Challenge01
//
//  Created by Vitor Muniz on 12/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import "AulaSutenBemolViewController.h"

@interface AulaSutenBemolViewController ()

@end

@implementation AulaSutenBemolViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.contContinue = 0;
    
    [self.textoAula setTextColor:[UIColor colorWithRed:255.0/255.0 green:140.0/255.0 blue:0 alpha:1.0]];
    self.textoAula.text = @"Além das 7 notas naturais existem as que são identificadas por um sustenido (#) ou bemol (b) associados a essas notas. A menor distância entre duas notas na música ocidental, é conhecida pelo nome de semitom.";
    [self.textoAula setSelectable:NO];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)continuar:(UIButton *)sender {
    self.contContinue ++;
    if(self.contContinue == 1){
        self.textoAula.text = @"O sustenido representa o aumento e o bemol a diminuição de um semitom à nota natural. A nota que fica entre duas notas naturais é classificada como sustenido da nota mais grave e bemol da nota mais aguda. Ex: Dó # = Ré b.";
        
    }else if (self.contContinue == 2){
        [self performSegueWithIdentifier:@"segueNotasSusten" sender:self];
    }
    
}
@end
