//
//  Exercicio1SonsViewController.m
//  Challenge01
//
//  Created by Vitor Muniz on 05/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import "Exercicio1SonsViewController.h"

@interface Exercicio1SonsViewController ()

@end

@implementation Exercicio1SonsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configuraBotoes];
    
    self.numAcertos = 0;
    self.numErros = 0;
    
    self.botaoContinuar.hidden = YES;
    
    self.nomeNotas = @[@"dó",@"ré",@"mi",@"fá",@"sol",@"lá",@"si"];

    self.cont = 0;
    
    self.reproducaoAudio = [[ReproducaoAudio alloc] initWithArray:@[@"C",@"D",@"E",@"F",@"G",@"A",@"B"] andNumeroDaOitava:@"2"];
    
    self.fase1 = [[Fase1 alloc] initWithNumeroDoExercicio:self.numeroDaFase];

    self.indexRespostaCerta = [[self.fase1.respostasExerciciosDeSom objectAtIndex:0] intValue];
    

    if (self.numeroDaFase == 1) { //exercicio sem sustenido e bemol
        
        self.respostaCerta = [self.nomeNotas objectAtIndex:self.indexRespostaCerta];
        self.indexNotaDeReferencia = [[self.fase1.notasDeReferencia objectAtIndex:0] intValue];
        [self.botaoDeReferencia setTitle: [NSString stringWithFormat:@"Som de referência: %@", [self.nomeNotas objectAtIndex:self.indexNotaDeReferencia]] forState:UIControlStateNormal];
        
        [self escreverNosBotoes:[[[self.fase1.botaoPorNota objectAtIndex:0] objectAtIndex:0]intValue] nomeNota:[self.nomeNotas objectAtIndex: [[self.fase1.respostasExerciciosDeSom objectAtIndex:0] intValue]]];
        [self escreverNosBotoes:[[[self.fase1.botaoPorNota objectAtIndex:0] objectAtIndex:1]intValue] nomeNota:[self.nomeNotas objectAtIndex: [[[self.fase1.opcoesErradas objectAtIndex:0] objectAtIndex:0]intValue]]];
        [self escreverNosBotoes:[[[self.fase1.botaoPorNota objectAtIndex:0] objectAtIndex:2]intValue] nomeNota:[self.nomeNotas objectAtIndex: [[[self.fase1.opcoesErradas objectAtIndex:0] objectAtIndex:1]intValue]]];
    

    }else if (self.numeroDaFase == 2){ //exercicio com Sustenido e Bemol
        
        self.nomeNotasSustenBemol = @[@"DÓ#", @"RÉ#",@"FÁ#",@"SOL#",@"LÁ#"];
        
        self.respostaCerta = [self.nomeNotasSustenBemol objectAtIndex:self.indexRespostaCerta];
        
        self.reproducaoAudioSustenBemol = [[ReproducaoAudio alloc] initWithArray:@[@"C#",@"D#",@"F#",@"G#",@"A#" ] andNumeroDaOitava:@"2"];
        //self.nomeNotasSustenBemol = @[@"DÓ# = RÉb", @"RÉ# = MIb",@"FÁ# = SOLb",@"SOL# = LÁb",@"LÁ# = SIb"];

        [self escreverNosBotoes:[[[self.fase1.botaoPorNota objectAtIndex:0] objectAtIndex:0]intValue] nomeNota:[self.nomeNotasSustenBemol objectAtIndex: [[self.fase1.respostasExerciciosDeSom objectAtIndex:0] intValue]]];
        [self escreverNosBotoes:[[[self.fase1.botaoPorNota objectAtIndex:0] objectAtIndex:1]intValue] nomeNota:[self.nomeNotasSustenBemol objectAtIndex: [[[self.fase1.opcoesErradas objectAtIndex:0] objectAtIndex:0]intValue]]];
        [self escreverNosBotoes:[[[self.fase1.botaoPorNota objectAtIndex:0] objectAtIndex:2]intValue] nomeNota:[self.nomeNotasSustenBemol objectAtIndex: [[[self.fase1.opcoesErradas objectAtIndex:0] objectAtIndex:1]intValue]]];
        
        self.indexNotaDeReferencia = 5;
        [self.botaoDeReferencia setTitle:[NSString stringWithFormat:@"Som de referência: %@",[self.nomeNotas objectAtIndex: self.indexNotaDeReferencia]] forState:UIControlStateNormal];

    }
    
}

-(void)configuraBotoes{
//    self.nota1.layer.borderWidth = 2;
    self.nota1.layer.cornerRadius = 8;
//    self.nota1.layer.borderColor = [[UIColor grayColor] CGColor];
//    
//    self.nota2.layer.borderWidth = 2;
    self.nota2.layer.cornerRadius = 8;
//    self.nota2.layer.borderColor = [[UIColor grayColor] CGColor];
//    
//    self.nota3.layer.borderWidth = 2;
    self.nota3.layer.cornerRadius = 8;
//    self.nota3.layer.borderColor = [[UIColor grayColor] CGColor];
//
//    
//    self.botaoDeReferencia.layer.borderWidth = 2;
    self.botaoDeReferencia.layer.cornerRadius = 8;
 //   self.botaoDeReferencia.layer.borderColor = [[UIColor grayColor] CGColor];

}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

-(void) escreverNosBotoes: (int)i nomeNota:(NSString *) nomeNota{
    if(i == 0){
        [self.nota1 setTitle: nomeNota forState:UIControlStateNormal];
    }else if (i == 1){
        [self.nota2 setTitle: nomeNota forState:UIControlStateNormal];
    }else {
        [self.nota3 setTitle: nomeNota forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)nota1Selecionada:(UIButton *)sender {
    if ([sender.titleLabel.text isEqualToString:self.respostaCerta]) {
        self.numAcertos ++;
        [self configuraBotaoAposResposta:0];
    }
    else{
        self.numErros ++;
        [self configuraBotaoAposResposta:1];
    }
}

- (IBAction)nota2Selecionada:(UIButton *)sender {

    if ([sender.titleLabel.text isEqualToString:self.respostaCerta]) {
        self.numAcertos ++;
        [self configuraBotaoAposResposta:0];
    }
    else{
        self.numErros ++;
        [self configuraBotaoAposResposta:1];
    }
}
- (IBAction)nota3Selecionada:(UIButton *)sender {
    if ([sender.titleLabel.text isEqualToString:self.respostaCerta]) {
        self.numAcertos ++;
        [self configuraBotaoAposResposta:0];
    }
    else{
        self.numErros ++;
        [self configuraBotaoAposResposta:1];
    }
}

-(void)configuraBotaoAposResposta: (int)resultado{
    self.botaoContinuar.hidden = NO;
    
    UIColor *vermelho = [UIColor colorWithRed: 227.0/255.0 green:69.0/255.0 blue:34.0/255.0 alpha:1.0];
    UIColor *verde = [UIColor colorWithRed:32.0/255.0 green:176.0/255.0 blue:87.0/255.0 alpha:1.0];
    
    if (resultado == 0) {
        self.botaoContinuar.backgroundColor = verde;
        [self.botaoContinuar setTitle:@"Você acertou! Continuar!" forState:UIControlStateNormal];
    }else {
        self.botaoContinuar.backgroundColor = vermelho;
         [self.botaoContinuar setTitle: [NSString stringWithFormat: @"Errado. Nota correta: %@. Continue." , self.respostaCerta] forState:UIControlStateNormal];
    }
    self.nota1.enabled = NO;
    self.nota2.enabled = NO;
    self.nota3.enabled = NO;
    
    NSLog(@"%d",self.numAcertos);
    NSLog(@"%d",self.numErros);

}


- (IBAction)botaoContinuarClick:(UIButton *)sender {
    
    self.cont++;
    
    if (((self.cont == 7)&&(self.numeroDaFase == 1)) || ((self.cont == 5)&&(self.numeroDaFase == 2))) {
        
        [self performSegueWithIdentifier:@"segueResultado" sender:self];
        
    }else{

        
        self.indexRespostaCerta = [[self.fase1.respostasExerciciosDeSom objectAtIndex:self.cont] intValue];
        
        NSLog(@"%@",self.respostaCerta);
        
        if (self.numeroDaFase == 1) {
        
            self.indexNotaDeReferencia = [[self.fase1.notasDeReferencia objectAtIndex:self.cont] intValue];
            [self.botaoDeReferencia setTitle: [NSString stringWithFormat:@"Som de referência: %@",[self.nomeNotas objectAtIndex:self.indexNotaDeReferencia]] forState:UIControlStateNormal];

            self.respostaCerta = [self.nomeNotas objectAtIndex:self.indexRespostaCerta];

            [self escreverNosBotoes:[[[self.fase1.botaoPorNota objectAtIndex:self.cont] objectAtIndex:0]intValue] nomeNota:[self.nomeNotas objectAtIndex: [[self.fase1.respostasExerciciosDeSom objectAtIndex:self.cont] intValue]]];
            
            [self escreverNosBotoes:[[[self.fase1.botaoPorNota objectAtIndex:self.cont] objectAtIndex:1]intValue] nomeNota:[self.nomeNotas objectAtIndex: [[[self.fase1.opcoesErradas objectAtIndex:self.cont] objectAtIndex:0]intValue]]];
            
            [self escreverNosBotoes:[[[self.fase1.botaoPorNota objectAtIndex:self.cont] objectAtIndex:2]intValue] nomeNota:[self.nomeNotas objectAtIndex: [[[self.fase1.opcoesErradas objectAtIndex:self.cont] objectAtIndex:1]intValue]]];
        
        } else {
            
            self.respostaCerta= [self.nomeNotasSustenBemol objectAtIndex:self.indexRespostaCerta];
            
            [self escreverNosBotoes:[[[self.fase1.botaoPorNota objectAtIndex:self.cont] objectAtIndex:0]intValue] nomeNota:[self.nomeNotasSustenBemol objectAtIndex: [[self.fase1.respostasExerciciosDeSom objectAtIndex:self.cont] intValue]]];
            
            [self escreverNosBotoes:[[[self.fase1.botaoPorNota objectAtIndex:self.cont] objectAtIndex:1]intValue] nomeNota:[self.nomeNotasSustenBemol objectAtIndex: [[[self.fase1.opcoesErradas objectAtIndex:self.cont] objectAtIndex:0]intValue]]];
            
            [self escreverNosBotoes:[[[self.fase1.botaoPorNota objectAtIndex:self.cont] objectAtIndex:2]intValue] nomeNota:[self.nomeNotasSustenBemol objectAtIndex: [[[self.fase1.opcoesErradas objectAtIndex:self.cont] objectAtIndex:1]intValue]]];
        }
        
        self.botaoContinuar.hidden = YES;
        [self.botaoContinuar setTitle:@"" forState:UIControlStateNormal];
        
        self.nota1.enabled = YES;
        self.nota2.enabled = YES;
        self.nota3.enabled = YES;
    }
        
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString: @"segueResultado"]) {
        ModalResultadoViewController *modalResultado = [segue destinationViewController];
        NSLog(@"%d, %d", self.numAcertos, self.numErros);
        modalResultado.numAcertos = self.numAcertos;
        modalResultado.numErros = self.numErros;
    }

}


- (IBAction)reproduzirSom:(UIButton *)sender {
    

    if (sender.tag == 0) {
        [self.reproducaoAudio playAudio:self.indexNotaDeReferencia];
    }else {
        if (self.numeroDaFase == 1) {
            [self.reproducaoAudio playAudio:self.indexRespostaCerta];
        }else {
            [self.reproducaoAudioSustenBemol playAudio:self.indexRespostaCerta];
        }
    }
}
@end
