//
//  ViewController.m
//  Challenge01
//
//  Created by Vitor Muniz on 03/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)beginnerTap:(UITapGestureRecognizer *)sender {
    //reconhecimento de toque na uiview
    NSLog(@"pegando o gesto");
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

@end
