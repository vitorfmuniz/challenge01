//
//  Exercicio1SonsViewController.h
//  Challenge01
//
//  Created by Vitor Muniz on 05/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReproducaoAudio.h"
#import "Fase1.h"
#import "ModalResultadoViewController.h"

@interface Exercicio1SonsViewController : UIViewController

@property int cont;
@property NSString *respostaCerta;
@property NSArray *nomeNotas;
@property NSArray *nomeNotasSustenBemol;
@property Fase1 *fase1;
@property ReproducaoAudio *reproducaoAudio;
@property ReproducaoAudio *reproducaoAudioSustenBemol;
@property NSUInteger indexRespostaCerta;
@property NSUInteger indexNotaDeReferencia;
@property int numeroDaFase;
@property int numAcertos;
@property int numErros;


@property (weak, nonatomic) IBOutlet UIButton *nota1;
@property (weak, nonatomic) IBOutlet UIButton *nota2;
@property (weak, nonatomic) IBOutlet UIButton *nota3;
@property (weak, nonatomic) IBOutlet UIButton *botaoDeReferencia;
@property (weak, nonatomic) IBOutlet UIButton *botaoContinuar;
@property (weak, nonatomic) IBOutlet UIButton *botaoNotaCorreta;



- (IBAction)nota1Selecionada:(UIButton *)sender;
- (IBAction)nota2Selecionada:(UIButton *)sender;
- (IBAction)nota3Selecionada:(UIButton *)sender;
- (IBAction)botaoContinuarClick:(UIButton *)sender;
- (IBAction)reproduzirSom:(UIButton *)sender;

-(void)configuraBotoes;
-(void)configuraBotaoAposResposta: (int)resultado;



@end
