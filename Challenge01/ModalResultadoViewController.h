//
//  ModalResultadoViewController.h
//  Challenge01
//
//  Created by Vitor Muniz on 12/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ModalResultadoViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *labelAcertos;
@property (weak, nonatomic) IBOutlet UILabel *labelErros;
@property (weak, nonatomic) IBOutlet UITextView *textAreaResultado;
@property int numAcertos;
@property int numErros;

@end
