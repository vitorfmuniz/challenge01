//
//  TelaDeSonsFase1ViewController.h
//  Challenge01
//
//  Created by Vitor Muniz on 05/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "ReproducaoAudio.h"
#import "Exercicio1SonsViewController.h"

@interface TelaDeSonsFase1ViewController : UIViewController

@property (strong, nonatomic) ReproducaoAudio *reproducaoAudio;



- (IBAction)reproduzirSom:(UIButton *)sender;


@end
