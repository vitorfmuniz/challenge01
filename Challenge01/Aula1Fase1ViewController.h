//
//  AulasComTextoViewController.h
//  Challenge01
//
//  Created by Vitor Muniz on 04/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Aula1Fase1ViewController : UIViewController
- (IBAction)Continue:(UIButton *)sender;
- (IBAction)buttonHomeAction:(UIButton *)sender;

@end
