//
//  CustomTableViewCell.h
//  Challenge01
//
//  Created by Vitor Muniz on 04/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imagem;
@property (weak, nonatomic) IBOutlet UILabel *descricao;


@end
