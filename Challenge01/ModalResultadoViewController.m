//
//  ModalResultadoViewController.m
//  Challenge01
//
//  Created by Vitor Muniz on 12/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import "ModalResultadoViewController.h"

@interface ModalResultadoViewController ()

@end

@implementation ModalResultadoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configurarTextAreaResultado];
    
    self.labelAcertos.text = [NSString stringWithFormat:@"%d", self.numAcertos];
    self.labelErros.text = [NSString stringWithFormat:@"%d", self.numErros];

    if ([self.labelAcertos.text intValue] > [self.labelErros.text intValue]) {
        self.textAreaResultado.text = @"O número de acertos foi maior que o de erros. Parabéns!";
    }else{
        self.textAreaResultado.text = @"O número de erros foi maior que o de acertos. Tente Novamente.";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)configurarTextAreaResultado{

    [self.textAreaResultado setFont:[UIFont systemFontOfSize:19.0]];
    self.textAreaResultado.textColor = [UIColor colorWithRed:255.0/255.0 green: 140.0/255.0 blue:0.0/255.0 alpha:1.0];
    self.textAreaResultado.textAlignment = NSTextAlignmentJustified;
    self.textAreaResultado.selectable = NO;
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
