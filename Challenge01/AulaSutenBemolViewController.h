//
//  AulaSutenBemolViewController.h
//  Challenge01
//
//  Created by Vitor Muniz on 12/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AulaSutenBemolViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *textoAula;
@property (weak, nonatomic) IBOutlet UIButton *botaoContinuar;
@property int contContinue;

- (IBAction)continuar:(UIButton *)sender;
@end
