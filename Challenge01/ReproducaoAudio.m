//
//  ReproducaoAudio.m
//  Challenge01
//
//  Created by Vitor Muniz on 11/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import "ReproducaoAudio.h"

@implementation ReproducaoAudio 

- (instancetype)initWithArray:(NSArray <NSString *> *) array andNumeroDaOitava: (NSString *)numeroDaOitava {
    
    self = [super init];
    if (self) {
        NSURL *url;
        AVAudioPlayer *musicPlayer;
        self.sons = [NSMutableArray array];
        for (NSString * string in array) {
            url = [[NSBundle mainBundle] URLForResource:[NSString stringWithFormat:@"%@%@", numeroDaOitava, string] withExtension:@"wav" subdirectory:@"PIANO KEYS"];
            musicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
            [self.sons addObject:musicPlayer];
        }
    }
    return self;
}

-(void)resetAudio:(NSInteger)index{
    [self.sons[self.index]  stop];
    ((AVAudioPlayer *)[self.sons objectAtIndex:index]).currentTime = 0;
}

-(void)playAudio:(NSInteger) index{
    [self.sons[self.index]  stop];
    ((AVAudioPlayer *)[self.sons objectAtIndex:index]).currentTime = 0;
    self.index = index;
    [((AVAudioPlayer *)[self.sons objectAtIndex:index]) play];
}


@end
