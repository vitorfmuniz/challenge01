//
//  Fase1.h
//  Challenge01
//
//  Created by Vitor Muniz on 06/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fase1 : NSObject

@property int numeroDoExercicio;

@property NSMutableArray *respostasExerciciosDeSom; //[3,4,5,2,1,0,6]
@property NSMutableArray *notasDeReferencia; //[4,1,6,0,2,5,3]
@property NSMutableArray *botaoPorNota;//[[1,2,0],[2,0,1],...]
@property NSMutableArray *opcoesErradas;//[[2,6],[5,0],...]



-(void) sortearNumeros: (int)quantNumeros;
-(void) sortearExercicios: (int)quantNumeros;
-(instancetype)initWithNumeroDoExercicio: (int) numeroDoExercicio;



@end
