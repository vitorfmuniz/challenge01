//
//  TelaDeSonsSutenBemolViewController.m
//  Challenge01
//
//  Created by Vitor Muniz on 12/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import "TelaDeSonsSutenBemolViewController.h"

@interface TelaDeSonsSutenBemolViewController ()

@end

@implementation TelaDeSonsSutenBemolViewController

- (void)viewDidLoad {
    [super viewDidLoad];
       self.reproducaoAudio = [[ReproducaoAudio alloc] initWithArray:@[@"C#",@"D#",@"F#",@"G#",@"A#" ] andNumeroDaOitava:@"2"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

- (IBAction)reproduzirSom:(UIButton *)sender {
    
      [self.reproducaoAudio playAudio:sender.tag];
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
