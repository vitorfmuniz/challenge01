//
//  LicoesViewController.h
//  Challenge01
//
//  Created by Vitor Muniz on 04/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Exercicio1SonsViewController.h"
#import "ExercicioCapturaDeSom1ViewController.h"
@interface LicoesViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *configureButton;
@property NSMutableArray *imagensBotoes;

@end
