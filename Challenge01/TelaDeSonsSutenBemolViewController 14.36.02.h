//
//  TelaDeSonsSutenBemolViewController.h
//  Challenge01
//
//  Created by Vitor Muniz on 12/05/16.
//  Copyright © 2016 Vitor Muniz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReproducaoAudio.h"
@interface TelaDeSonsSutenBemolViewController : UIViewController

@property ReproducaoAudio *reproducaoAudio;


- (IBAction)reproduzirSom:(UIButton *)sender;


@end
